const mongoose = require("mongoose");

const productSchema = new mongoose.Schema(
	{
		name: {
			type: String,
			required: [true, "Product is required"]
		},

		description: {
			type: String,
			required: [true, "Description is required"]
		},

		price : {
			type: Number,
			required: [true, "Price is required"]
		},

		isActive:{
			type: Boolean,
			default: true
		},

		createdOn : {
			type: Date,
			// The "new Date()" expression instantiates a new "Date" that the current date and time whenever a course is created in our database
			default: new Date()
		},

		Buyers : [
			{
				userId: {
					type: String,
					required: [true, "UserId is required"]
				},
				BuyersDate: {
					type: Date,
					default: new Date()
				}
			}
		]
	}
) 

module.exports = mongoose.model("Product", courseSchema);


